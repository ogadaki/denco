#!/bin/bash

commit_hashes_file=$1
tag_base_name=$2

# An `awk` program which does the following:
#   If stdin is:
#     0b6a1ad init my demo app with some content
#     af18a51 create my demo app
#   and `tag_base_name` is "step", this awk program produces:
#     git tag step-0 0b6a1ad
#     git tag step-1 af18a51
commit_hashes_2_git_tag_commands="
  BEGIN { tag_number = 0 } {
    commit_hash = \$1
    printf \"git tag $tag_base_name-%d %s\n\", tag_number, commit_hash
    tag_number++
  }
"

cat "$commit_hashes_file" \
  | awk "$commit_hashes_2_git_tag_commands" \
  | xargs -n1 -0 bash -c # actually execute the `git tag` commands
