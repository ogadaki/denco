#!/bin/bash

set -e
trap 'echo "$0: unexpected error on line $LINENO"' ERR

if [ "$1" = "--help" ]; then
  echo 'Usage: '"$0"' <tag_base_name> <action>'
  echo ''
  echo 'Script to move from a numbered git tag to another. Tags must be in the form'
  echo '"v-12" or "mytag-89", with only one hyphen. Numbered tags start at 0 (i.e.'
  echo '"mytag-0").'
  echo ''
  echo 'Command "tag mytag +" moves one tag forward and "tag mytag -" moves backward.'
  echo ''
  echo "The git's head is positionned one tag before the tag corresponding to the"
  echo 'content of the files. This way, one can show differences between the two tags'
  echo 'using her favorite git diff tools (in a text editor, for example).'
  echo ''
  echo 'The command "tag mytag start" bring contents to "mytag-1" and git head to'
  echo '"mytag-0".'
  exit
fi

if [ "$#" != 2 ]; then
  echo "$0: error: you must give two arguments."
  exit 1
fi

tag_base_name=$1

if [ "$2" = "start" ]; then
  git checkout -f "$tag_base_name-1" > /dev/null 2>&1
  echo "$tag_base_name-1: $(git log -n 1 --pretty=format:"%s%n")"
  git reset "$tag_base_name-0" > /dev/null 2>&1
  echo "HEAD is now at tag $tag_base_name-0 and files are at tag $tag_base_name-1"
  exit 0
fi

if [ "x$2" != "x+" ] && [ "x$2" != "x-" ]; then
  echo "$0: error: second argument must be either 'start', '+' or '-'."
  exit 1
fi
sign=$2

tag_pattern="$tag_base_name-*"
current_tag_for_head=$(
  git describe --tags --match "$tag_pattern" 2> /dev/null \
) || {
  echo "$0: error: no tag found with pattern \"$tag_pattern\""
  exit 2
} 

head_tag_number=$(echo "$current_tag_for_head" | cut -d "-" -f 2)

current_tag_for_files=$tag_base_name-$(( $head_tag_number + 1 ))
new_tag_for_files=$tag_base_name-$(( $head_tag_number $sign 1 + 1 ))
new_tag_for_head=$tag_base_name-$(( $head_tag_number $sign 1 ))

# Check tags exitence. The folloing command exit with error if tag is missing.
git show-ref --tags --quiet --verify -- "refs/tags/$current_tag_for_files" || {
  echo "$0: error: bad current state: curent tag is '$current_tag_for_head' but there is no following tag ('$current_tag_for_files')."
  exit 3
}
git show-ref --tags --quiet --verify -- "refs/tags/$new_tag_for_files" || {
  echo "already at last tag, do nothing"
  exit 0
}
git show-ref --tags --quiet --verify -- "refs/tags/$new_tag_for_head" || {
  echo "already at first tag, do nothing"
  exit 0
}

# Set HEAD at current tag for files
git reset "$current_tag_for_files" > /dev/null 2>&1

# Checkout files content at requested tag
git checkout -f "$new_tag_for_files" > /dev/null 2>&1

# Show the corresponding log message before switching to prev tag
echo "$new_tag_for_files: $(git log -n 1 --pretty=format:"%s%n")"

# Set HEAD at one tag before files content
git reset "$new_tag_for_head" > /dev/null 2>&1
