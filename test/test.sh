#!/bin/bash

here="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
set -e

trap 'echo "error (line $LINENO)."' ERR

working_dir=$(realpath "$here")/tmp/work

tag=$(realpath "$here/../tag")

echo -n "The file for the tag tool must exist... "
[ -e "$tag" ]
echo "ok."

hashes2tags=$(realpath "$here/../hashes2tags.sh")

echo -n "The file for the hashes2tags tool must exist... "
[ -e "$hashes2tags" ]
echo "ok."

if [ -e "$working_dir" ]; then
  rm -rf "$working_dir"
fi
mkdir -p "$working_dir"

cd "$working_dir"

# Prepare data for testing
{
  git init

  touch index.html
  git add index.html
  git commit -a -m 'create my demo app'
  first_hash=$( git rev-parse --short HEAD )

  echo My demo app > index.html
  git commit -a -m 'init my demo app with some content'
  second_hash=$( git rev-parse --short HEAD )

  echo 'with a nice feature' >> index.html
  git commit -a -m 'add some nice feature'
  third_hash=$( git rev-parse --short HEAD )
} > /dev/null


hashes_file=hashes.txt
git log --oneline | tac > "$hashes_file"

tags_base_name=step

$hashes2tags "$hashes_file" $tags_base_name


echo -n "After tags creation, there must be 3 tags... "
[ $( git tag | wc -l ) -eq 3 ]
echo "ok."


echo -n "Current hash must be the third one... "
[ "$( git rev-parse --short HEAD )" == "$third_hash" ]
echo "ok."


$tag $tags_base_name start > /dev/null
echo -n "After a 'tag step start', current hash must be the first one... "
[ "$( git rev-parse --short HEAD )" == "$first_hash" ]
echo "ok."
echo -n "But 'index.html' content must be those of the second commit... "
[ "$( cat index.html )" == "My demo app" ]
echo "ok."


$tag $tags_base_name + > /dev/null
echo -n "After a 'tag step +', the hash must be the second one... "
[ "$( git rev-parse --short HEAD )" == "$second_hash" ]
echo "ok."
echo -n "And 'index.html' content must be those of the third commit... "
[ "$( cat index.html )" == "My demo app
with a nice feature" ]
echo "ok."


$tag $tags_base_name + > /dev/null
echo -n "When at last step, after a 'tag step +', the hash still must be the second one... "
[ "$( git rev-parse --short HEAD )" == "$second_hash" ]
echo "ok."


$tag $tags_base_name - > /dev/null
echo -n "After a 'tag step -', the hash must be the first one... "
[ "$( git rev-parse --short HEAD )" == "$first_hash" ]
echo "ok."
echo -n "And 'index.html' content must be those of the second commit... "
[ "$( cat index.html )" == "My demo app" ]
echo "ok."


$tag $tags_base_name - > /dev/null
echo -n "When at first step, after a 'tag step -', the hash still must be the first one... "
[ "$( git rev-parse --short HEAD )" == "$first_hash" ]
echo "ok."


git tag onetag-0
echo -n "The tag command fails if the considered tag has no follower, like a unique tag... "
failed=no
output=$( $tag onetag + ) || failed=yes
[ $failed == yes ]
[ "$output" == "$tag: error: bad current state: curent tag is 'onetag-0' but there is no following tag ('onetag-1')." ]
[ "$( git rev-parse --short HEAD )" == "$first_hash" ]
echo "ok."


echo -n "Unknown base tag name must make tag command fails... "
failed=no
output=$( $tag unknown_tag + ) || failed=yes
[ $failed == yes ]
[ "$output" == "$tag: error: no tag found with pattern \"unknown_tag-*\"" ]
echo "ok."


echo -n "Command tag must fail if its second argument is not 'start', '+' or '-'... "
failed=no
output=$( $tag $tags_base_name bad_2nd_arg) || failed=yes
[ $failed == yes ]
[ "$output" == "$tag: error: second argument must be either 'start', '+' or '-'." ]
echo "ok."

echo -n "Command tag must fail if there is only one argument... "
failed=no
output=$( $tag $tags_base_name ) || failed=yes
[ $failed == yes ]
[ "$output" == "$tag: error: you must give two arguments (the tag base name and the action)." ]
echo "ok."

echo -n "Command tag must fail if there is no argument... "
failed=no
output=$( $tag ) || failed=yes
[ $failed == yes ]
[ "$output" == "$tag: error: you must give two arguments (the tag base name and the action)." ]
echo "ok."



echo "-> Tests ok."